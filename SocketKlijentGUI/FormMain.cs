using System.Net.Sockets;
using System.Text;

namespace SocketKlijentGUI {
  public partial class FormMain : Form {
    public FormMain() {
      InitializeComponent();
    }

        private void btnSend_Click(object sender, EventArgs e) {
            TcpClient klijent = new TcpClient(tbServerIP.Text, int.Parse(tbServerPort.Text));
            Byte[] poruka = Encoding.ASCII.GetBytes(tbMessage.Text);
            NetworkStream stream = klijent.GetStream();
            stream.Write(poruka, 0, poruka.Length);
            stream.Close();
            klijent.Close();
        }
    }
}